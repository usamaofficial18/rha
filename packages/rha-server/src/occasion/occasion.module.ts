import { Module } from '@nestjs/common';
import { OccasionService } from './occasion.service';
import { OccasionController } from './occasion.controller';
import { DatabaseModule } from '../database/database.module';
import { Entities, EntityServices } from '../index';
import { OccasionAggregateService } from './occasion-aggregate/occasion-aggregate.service';

@Module({
  imports: [DatabaseModule],
  controllers: [OccasionController],
  providers: [
    OccasionService,
    ...Entities,
    ...EntityServices,
    OccasionAggregateService,
  ],
  exports: [...EntityServices],
})
export class OccasionModule {}
