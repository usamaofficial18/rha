import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { OCCASION } from './entities/occasion.schema';
import { Occasion } from './entities/occasion.interface';
import { OccasionDto } from './entities/occasion.dto';
import { Observable, from } from 'rxjs';

@Injectable()
export class OccasionService {
  constructor(
    @Inject(OCCASION) public readonly occasionModel: Model<Occasion>,
  ) {}

  create(createOccasionDto: OccasionDto): Observable<Occasion> {
    return from(this.occasionModel.create(createOccasionDto));
  }

  find(query?): Observable<any> {
    return from(this.occasionModel.find(query));
  }

  findOne(uuid): Observable<Occasion> {
    return from(this.occasionModel.findOne(uuid));
  }

  update(query, params): Observable<any> {
    return from(this.occasionModel.updateOne(query, params));
  }

  remove(uuid): Observable<any> {
    return from(this.occasionModel.deleteOne(uuid));
  }
}
