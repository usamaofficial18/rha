import { Document } from 'mongoose';

export class Occasion extends Document {
  uuid: string;
  title: string;
  description: string;
  status: boolean;
  date: Date;
  startTime: string;
  endTime: string;
}
