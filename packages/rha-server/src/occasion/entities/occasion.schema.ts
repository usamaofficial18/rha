import * as mongoose from 'mongoose';
import { v4 as uuidv4 } from 'uuid';

const schema = new mongoose.Schema(
  {
    uuid: { type: String, default: uuidv4, index: true, unique: true },
    title: { type: String, index: true },
    description: String,
    status: { type: Boolean, default: 1 },
    date: Date,
    startTime: String,
    endTime: String,
  },
  { collection: 'event', versionKey: false },
);

export const Occasion = schema;

export const OCCASION = 'Occasion';
