import { Test, TestingModule } from '@nestjs/testing';
import { OccasionAggregateService } from './occasion-aggregate.service';

describe('OccasionAggregateService', () => {
  let service: OccasionAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [OccasionAggregateService],
    }).compile();

    service = module.get<OccasionAggregateService>(OccasionAggregateService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
