import { Injectable } from '@nestjs/common';
import { from, map, of, switchMap } from 'rxjs';
import { OccasionDto } from '../entities/occasion.dto';
import { OccasionService } from '../occasion.service';
import * as moment from 'moment';

@Injectable()
export class OccasionAggregateService {
  constructor(private readonly occasionService: OccasionService) {}

  create(occasion: OccasionDto) {
    return from(this.occasionService.create(occasion));
  }

  list() {
    return this.occasionService
      .find({ status: true })
      .pipe(
        map((data) => this.setEventStatus(data)),
        switchMap(res=>{
        return this.occasionService.find();
      }))
  }

  find(uuid: string) {
    return from(this.occasionService.findOne({ uuid }));
  }

  update(occasion: OccasionDto) {
    return from(
      this.occasionService.update({ uuid: occasion.uuid }, { $set: occasion }),
    );
  }

  remove(uuid: string) {
    return from(this.occasionService.remove({ uuid }));
  }

  setEventStatus(occasion: OccasionDto[]) {
    const today = moment(new Date());
    occasion.forEach((element: OccasionDto) => {
      const endDateTime = moment(
        new Date(
          element.date.getFullYear(),
          element.date.getMonth(),
          element.date.getDate(),
          parseInt(moment(element.endTime, 'hh:mm A').format('HH')),
          parseInt(moment(element.endTime, 'hh:mm A').format('mm')),
        ),
      );
      if (endDateTime.isBefore(today)) {
        element.status = false;
        return this.update(element)
      }
    });
    return of({});
  }
}
