import { Provider } from '@nestjs/common';
import { Connection } from 'mongoose';
import { DATABASE_CONNECTION } from './database/database.provider';
import { Occasion, OCCASION } from './occasion/entities/occasion.schema';
import { OccasionService } from './occasion/occasion.service';
import { User, USER } from './user/entities/user.schema';

export const Entities: Provider[] = [
  {
    provide: OCCASION,
    useFactory: (connection: Connection) =>
      connection.model(OCCASION, Occasion),
    inject: [DATABASE_CONNECTION],
  },
  {
    provide: USER,
    useFactory: (connection: Connection) => connection.model(USER, User),
    inject: [DATABASE_CONNECTION],
  },
];

export const EntityServices: Provider[] = [OccasionService];
