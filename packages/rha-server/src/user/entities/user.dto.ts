import {
  IsDate,
  IsNumberString,
  IsOptional,
  IsString,
  IsUUID,
} from 'class-validator';
import { v4 as uuidv4 } from 'uuid';
import { UserRole } from './user.interface';

export class UserDto {
  @IsUUID()
  @IsOptional()
  uuid?: uuidv4;

  @IsString()
  @IsOptional()
  name?: string;

  @IsString()
  @IsOptional()
  phone?: string;

  @IsString()
  @IsOptional()
  email?: string;

  @IsString()
  @IsOptional()
  password?: string;

  @IsString()
  @IsOptional()
  role?: UserRole;

  @IsDate()
  @IsOptional()
  dateOfJoining?: Date;

  @IsNumberString()
  @IsOptional()
  drivesAttended?: number;
}
