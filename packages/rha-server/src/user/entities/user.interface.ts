import { Document } from 'mongoose';

export class User extends Document {
  uuid: string;
  name: string;
  phone: string;
  email: string;
  password: string;
  role: UserRole;
  dateOfJoining: Date;
  drivesAttended: number;
}

export enum UserRole {
  ADMIN = 'admin',
  VOLUNTEER = 'volunteer',
}
