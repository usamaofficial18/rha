import * as mongoose from 'mongoose';
import { v4 as uuidv4 } from 'uuid';

const schema = new mongoose.Schema(
  {
    uuid: { type: String, default: uuidv4, index: true, unique: true },
    name: { type: String, index: true },
    phone: { type: Number, unique: false },
    email: { type: String, unique: true },
    role: { type: String, default: 'user' },
    password: String,
    dateOfJoining: Date,
    drivesAttended: { type: Number, default: 0 },
  },
  { collection: 'user', versionKey: false },
);

export const User = schema;

export const USER = 'User';
