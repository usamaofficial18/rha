import { Module } from '@nestjs/common';
import { UserController } from './user.controller';
import { UserAggregateService } from './user-aggregate/user-aggregate.service';
import { UserService } from './user.service';
import { Entities, EntityServices } from '../index';
import { DatabaseModule } from '../database/database.module';
import { AuthModule } from 'src/auth/auth.module';

@Module({
  imports: [DatabaseModule, AuthModule],
  controllers: [UserController],
  providers: [
    UserService,
    ...Entities,
    ...EntityServices,
    UserAggregateService,
  ],
  exports: [...EntityServices, UserAggregateService],
})
export class UserModule {}
