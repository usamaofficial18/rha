import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { UserDto } from './entities/user.dto';
import { User } from './entities/user.interface';
import { USER } from './entities/user.schema';
import { Observable, from } from 'rxjs';

@Injectable()
export class UserService {
  constructor(@Inject(USER) public readonly userModel: Model<User>) {}

  create(userDto: UserDto): Observable<User> {
    return from(this.userModel.create(userDto));
  }

  list(): Observable<any> {
    return from(this.userModel.find());
  }

  findOne(uuid): Observable<User> {
    return from(this.userModel.findOne(uuid));
  }

  update(query, params): Observable<any> {
    return from(this.userModel.updateOne(query, params));
  }

  remove(uuid): Observable<any> {
    return from(this.userModel.deleteOne(uuid));
  }
}
