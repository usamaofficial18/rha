import { BadRequestException, Injectable } from '@nestjs/common';
import { Observable, from, throwError } from 'rxjs';
import { switchMap, map, catchError } from 'rxjs/operators';
import { AuthService } from 'src/auth/services/auth.service';
import { UserDto } from '../entities/user.dto';
import { User } from '../entities/user.interface';
import { UserService } from '../user.service';
import { EMAIL_EXISTS } from '../../constants/strings';

@Injectable()
export class UserAggregateService {
  constructor(
    private readonly userService: UserService,
    private readonly authService: AuthService,
  ) {}

  create(user: UserDto): Observable<any> {
    this.userService.findOne({ email: user.email }).subscribe((res) => {
      if (res) {
        throw new BadRequestException(EMAIL_EXISTS);
      }
    });

    return this.authService.hashPassword(user.password).pipe(
      switchMap((passwordHash: string) => {
        user.password = passwordHash;
        return from(this.userService.create(user)).pipe(
          map((createdUser: User) => {
            delete createdUser.password;
            return createdUser;
          }),
          catchError((err) => throwError(() => err)),
        );
      }),
    );
  }

  list(): Observable<User[]> {
    return from(this.userService.list()).pipe(
      map((users: User[]) => {
        const data = [];
        Object.assign(data, users);
        data.forEach(function (lala) {
          lala.password = undefined;
          console.log(lala.password);
        });
        return data;
      }),
    );
  }

  find(uuid: string): Observable<any> {
    return from(this.userService.findOne({ uuid })).pipe(
      map((user: User) => {
        console.log(user);
        delete user.password;
        console.log(user);

        return user;
      }),
    );
  }

  update(user: UserDto): Observable<any> {
    if (user.password) {
      return this.authService.hashPassword(user.password).pipe(
        switchMap((passwordHash: string) => {
          user.password = passwordHash;
          return from(
            this.userService.update({ uuid: user.uuid }, { $set: user }),
          );
        }),
      );
    } else {
      return from(this.userService.update({ uuid: user.uuid }, { $set: user }));
    }
  }

  remove(uuid: string): Observable<any> {
    return this.userService.remove({ uuid });
  }

  login(user: User): Observable<string> {
    return this.validateUser(user.email, user.password).pipe(
      switchMap((user: User) => {
        if (user) {
          return this.authService
            .generateJWT(user)
            .pipe(map((jwt: string) => jwt));
        } else {
          return 'Wrong Credentials';
        }
      }),
    );
  }

  validateUser(email: string, password: string): Observable<User | Object> {
    return this.findByEmail(email).pipe(
      switchMap((user: User) =>
        this.authService.comparePasswords(password, user.password).pipe(
          map((match: boolean) => {
            if (match) {
              delete user.password;
              return user;
            } else {
              throw Error;
            }
          }),
        ),
      ),
    );
  }

  findByEmail(email: string): Observable<User> {
    return from(this.userService.findOne({ email }));
  }
}
