import * as mongoose from 'mongoose';
import { Provider } from '@nestjs/common';

export const DATABASE_CONNECTION = 'DATABASE_CONNECTION';

export const DatabaseProvider: Provider = {
  provide: DATABASE_CONNECTION,
  useFactory: async (): Promise<mongoose.Connection> => {
    return await mongoose.createConnection(
      'mongodb://rha:admin@localhost:27017/rha'
    );
  },
};
