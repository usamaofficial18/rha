import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { AppComponent } from 'src/app/app.component';
import { DURATION, UNABLE_TO_FETCH_USERS } from 'src/app/constants/strings';
import { LIST_USERS } from 'src/app/constants/url-strings';
import { ListingService } from 'src/app/services/listing.service';
import { environment } from 'src/environments/environment';
import { User } from '../../interfaces/user';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.page.html',
  styleUrls: ['./list-users.page.scss'],
})
export class ListUsersPage implements OnInit {
  users: User[] = [];
  constructor(
    private readonly listingService: ListingService,
    private toastController: ToastController,
    private appComponent: AppComponent
  ) {}

  ngOnInit() {
    this.listEvents();
  }

  listEvents() {
    const url = environment.serverUrl + LIST_USERS;
    this.appComponent.getAccessToken().then((token) => {
      this.listingService.getRequest(url, undefined, token).subscribe({
        next: (success) => {
          this.users = success;
        },
        error: (error) => {
          console.log(error);
          this.presentToast(UNABLE_TO_FETCH_USERS);
        },
      });
    });
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: DURATION,
    });
    toast.present();
  }
}
