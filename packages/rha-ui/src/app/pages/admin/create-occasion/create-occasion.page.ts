import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ToastController } from '@ionic/angular';
import { ListingService } from '../../../services/listing.service';
import { environment } from '../../../../environments/environment';
import { Occasion } from '../../../interfaces/occasion';
import {
  CREATE_OCCASION,
  FETCH_OCCASION,
  GET_OCCASIONS,
  UPDATE_OCCASION,
} from '../../../constants/url-strings';
import {
  DURATION,
  EVENT_CREATED,
  EVENT_UPDATED,
  UNABLE_TO_CREATE_EVENT,
  UNABLE_TO_FETCH_EVENTS,
  UNABLE_TO_UPDATE_EVENT,
} from '../../../constants/strings';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'create-occasion',
  templateUrl: './create-occasion.page.html',
  styleUrls: ['./create-occasion.page.scss'],
})
export class CreateOccasionPage implements OnInit {
  form = new FormGroup({
    title: new FormControl(),
    description: new FormControl(),
    date: new FormControl(new Date()),
    startTime: new FormControl(),
    endTime: new FormControl(),
  });
  uuid: string = '';

  constructor(
    private readonly listingService: ListingService,
    public readonly toastController: ToastController,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private location: Location
  ) {}

  ngOnInit() {
    this.uuid = this.route.snapshot.params.uuid;
    if (this.uuid) {
      const url = environment.serverUrl + FETCH_OCCASION + this.uuid;
      this.listingService.getRequest(url).subscribe({
        next: (payload) => {
          this.setFormValues(payload);
        },
        error: (error) => {
          this.presentToast(UNABLE_TO_FETCH_EVENTS);
        },
      });
    }
  }

  navigateBack() {
    this.location.back();
  }

  createEvent() {
    const url = environment.serverUrl + CREATE_OCCASION;

    this.listingService.postRequest(url, this.getFormvalues()).subscribe({
      next: (payload) => {
        this.form.reset();
        this.form.get('date').setValue(new Date());
        this.presentToast(EVENT_CREATED);
        this.router.navigate(['view-events']);
      },
      error: (error) => {
        console.log(error);
        this.presentToast(UNABLE_TO_CREATE_EVENT);
      },
    });
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: DURATION,
    });
    toast.present();
  }

  getFormvalues() {
    const occasion = {} as Occasion;
    occasion.title = this.form.get('title').value;
    occasion.description = this.form.get('description').value;
    occasion.date = new Date(this.form.get('date').value);
    occasion.startTime = this.form.get('startTime').value;
    occasion.endTime = this.form.get('endTime').value;
    return occasion;
  }

  setFormValues(payload: Occasion) {
    this.form.get('title').setValue(payload.title);
    this.form.get('description').setValue(payload.description);
    this.form.get('date').setValue(payload.date);
    this.form.get('startTime').setValue(payload.startTime);
    this.form.get('endTime').setValue(payload.endTime);
  }

  updateEvent() {
    const updatedPayload: Occasion = this.getFormvalues();
    updatedPayload.uuid = this.uuid;
    const url = environment.serverUrl + UPDATE_OCCASION;

    this.listingService.postRequest(url, updatedPayload).subscribe({
      next: (payload) => {
        this.form.reset();
        this.form.get('date').setValue(new Date());
        this.presentToast(EVENT_UPDATED);
        this.router.navigate(['view-events']);
      },
      error: (error) => {
        console.log(error);
        this.presentToast(UNABLE_TO_UPDATE_EVENT);
      },
    });
  }
}
