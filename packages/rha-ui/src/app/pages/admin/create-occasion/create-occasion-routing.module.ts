import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateOccasionPage } from './create-occasion.page';

const routes: Routes = [
  {
    path: '',
    component: CreateOccasionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateEventPageRoutingModule {}
