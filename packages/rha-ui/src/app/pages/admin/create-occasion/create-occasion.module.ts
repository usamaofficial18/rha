import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreateEventPageRoutingModule } from './create-occasion-routing.module';

import { CreateOccasionPage } from './create-occasion.page';
import { MaterialModule } from 'src/app/material.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreateEventPageRoutingModule,
    MaterialModule,
    ReactiveFormsModule,
  ],
  declarations: [CreateOccasionPage],
})
export class CreateOccasionPageModule {}
