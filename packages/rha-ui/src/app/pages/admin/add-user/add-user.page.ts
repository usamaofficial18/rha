import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ToastController } from '@ionic/angular';
import {
  DURATION,
  UNABLE_TO_ADD_USER,
  USER_ADDED,
} from 'src/app/constants/strings';
import { ADD_USER } from 'src/app/constants/url-strings';
import { ListingService } from 'src/app/services/listing.service';
import { environment } from 'src/environments/environment';
import { User } from '../../../interfaces/user';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.page.html',
  styleUrls: ['./add-user.page.scss'],
})
export class AddUserPage implements OnInit {
  form: FormGroup;
  constructor(
    private readonly listingService: ListingService,
    public readonly toastController: ToastController
  ) {}

  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl(),
      phone: new FormControl(),
      email: new FormControl(),
      password: new FormControl(),
      role: new FormControl(),
      dateOfJoining: new FormControl(),
    });
  }

  addUser() {
    const user = {} as User;
    user.name = this.form.get('name').value;
    user.phone = this.form.get('phone').value;
    user.email = this.form.get('email').value;
    user.password = this.form.get('password').value;
    user.role = this.form.get('role').value;
    user.dateOfJoining = new Date(this.form.get('dateOfJoining').value);
    const url = environment.serverUrl + ADD_USER;
    this.listingService.postRequest(url, user).subscribe({
      next: (success) => {
        this.form.reset();
        this.presentToast(USER_ADDED);
      },
      error: (error) => {
        console.log(error);
        this.presentToast(UNABLE_TO_ADD_USER);
      },
    });
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: DURATION,
    });
    toast.present();
  }
}
