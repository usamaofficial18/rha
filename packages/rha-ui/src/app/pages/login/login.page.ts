import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { LOGIN } from 'src/app/constants/url-strings';
import { User } from 'src/app/interfaces/user';
import { ListingService } from 'src/app/services/listing.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  hide = true;
  form = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required]),
  });

  constructor(
    private readonly listingService: ListingService,
    private readonly appComponent: AppComponent,
    private router: Router
  ) {}

  ngOnInit() {}

  login() {
    const user = {} as User;
    user.email = this.form.get('email').value;
    user.password = this.form.get('password').value;
    const url = environment.serverUrl + LOGIN;
    this.listingService.postRequest(url, user).subscribe({
      next: (success) => {
        this.appComponent.storeAccessToken(success.access_token);
        this.form.reset();
        this.router.navigate(['/'], { state: { data: success.access_token } });
      },
      error: (error) => {},
    });
  }
}
