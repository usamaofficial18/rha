import { Component, OnInit } from '@angular/core';
import { AppComponent } from 'src/app/app.component';
import { LIST_USERS } from 'src/app/constants/url-strings';
import { User } from 'src/app/interfaces/user';
import { ListingService } from 'src/app/services/listing.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-leaderboard',
  templateUrl: './leaderboard.page.html',
  styleUrls: ['./leaderboard.page.scss'],
})
export class LeaderboardPage implements OnInit {
  users: User[] = [];

  constructor(
    private readonly listingService: ListingService,
    private appComponent: AppComponent
  ) {}

  ngOnInit() {
    this.getUsers();
  }

  getUsers() {
    const url = environment.serverUrl + LIST_USERS;
    this.appComponent.getAccessToken().then((token) => {
      this.listingService.getRequest(url, undefined, token).subscribe({
        next: (users) => {
          this.users = users;
          this.users = this.users.sort(
            (a, b) => 0 - (a.drivesAttended > b.drivesAttended ? 1 : -1)
          );
          // this.users = this.users.sort();
          console.log(this.users);
        },
        error: (err) => {
          console.log(err);
        },
      });
    });
  }
}
