import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router, RouterEvent } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { DURATION, UNABLE_TO_FETCH_EVENTS } from 'src/app/constants/strings';
import { GET_OCCASIONS } from 'src/app/constants/url-strings';
import { Occasion } from 'src/app/interfaces/occasion';
import { ListingService } from 'src/app/services/listing.service';
import { environment } from 'src/environments/environment';
import { Location } from '@angular/common';

@Component({
  selector: 'app-view-occasions',
  templateUrl: './view-occasions.page.html',
  styleUrls: ['./view-occasions.page.scss'],
})
export class ViewOccasionsPage implements OnInit {
  activeOccasions: Occasion[];
  expiredOccasions: Occasion[];
  segment: string;

  constructor(
    private readonly listingService: ListingService,
    private toastController: ToastController,
    private router: Router,
    private location: Location
  ) {}

  ngOnInit() {
    

    this.router.events.subscribe({
      next: async (event: RouterEvent) => {
        if (event instanceof NavigationEnd) {
          this.segment = 'active';
    this.activeOccasions = [];
    this.expiredOccasions = [];
          this.listEvents();
        }
      },
    });
  }

  navigateBack() {
    this.location.back();
  }

  listEvents() {
    const url = environment.serverUrl + GET_OCCASIONS;
    this.listingService.getRequest(url).subscribe({
      next: (success) => {
        success.forEach((element) => {
          if (element.status) {
            this.activeOccasions.push(element);
          } else {
            this.expiredOccasions.push(element);
          }
        });
      },
      error: (error) => {
        console.log(error);
        this.presentToast(UNABLE_TO_FETCH_EVENTS);
      },
    });
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: DURATION,
    });
    toast.present();
  }

  segmentChanged(ev: any) {
    // console.log('Segment changed', ev);
  }
}
