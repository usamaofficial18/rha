import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewOccasionsPage } from './view-occasions.page';

const routes: Routes = [
  {
    path: '',
    component: ViewOccasionsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewOccasionsPageRoutingModule {}
