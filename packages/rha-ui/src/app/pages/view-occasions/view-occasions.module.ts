import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewOccasionsPageRoutingModule } from './view-occasions-routing.module';

import { ViewOccasionsPage } from './view-occasions.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViewOccasionsPageRoutingModule
  ],
  declarations: [ViewOccasionsPage]
})
export class ViewOccasionsPageModule {}
