import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ACCESS_TOKEN } from 'src/app/constants/strings';
import { User } from 'src/app/interfaces/user';
import { AuthService } from 'src/app/services/auth.service';
import { StorageService } from 'src/app/services/storage.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {
  loggedIn: boolean = false;
  token = '';
  user: string = '';
  constructor(
    private readonly storage: StorageService,
    private readonly authService: AuthService,
    private router: Router
  ) {}

  async ngOnInit() {
    this.token = await this.getToken();
    if (!this.token) {
      this.router.navigate(['/splash-screen']);
      this.loggedIn = false;
    } else {
      this.loggedIn = true;
    }
    this.user = await this.authService.getUserFromToken(this.token);
  }

  getToken() {
    return this.storage.getItem(ACCESS_TOKEN);
  }

  logout() {
    this.storage
      .removeItem(ACCESS_TOKEN)
      .then(() => this.router.navigate(['/splash-screen']));
  }
}
