import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import {
  DURATION,
  UNABLE_TO_ADD_USER,
  USER_ADDED,
} from 'src/app/constants/strings';
import { ADD_USER } from 'src/app/constants/url-strings';
import { User } from 'src/app/interfaces/user';
import { ListingService } from 'src/app/services/listing.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  hide = true;
  form = new FormGroup({
    name: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required]),
  });
  constructor(
    private readonly listingService: ListingService,
    private readonly toastController: ToastController,
    private readonly router: Router
  ) {}

  ngOnInit() {}

  register() {
    const user = {} as User;
    user.name = this.form.get('name').value;
    user.email = this.form.get('email').value;
    user.password = this.form.get('password').value;
    user.dateOfJoining = new Date();
    const url = environment.serverUrl + ADD_USER;
    this.listingService.postRequest(url, user).subscribe({
      next: (success) => {
        this.form.reset();
        this.presentToast(USER_ADDED);
        this.router.navigate(['/login']);
        console.log(success);
      },
      error: (error) => {
        console.log(error);
        this.presentToast(UNABLE_TO_ADD_USER);
      },
    });
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: DURATION,
    });
    toast.present();
  }
}
