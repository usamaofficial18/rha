import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { AppComponent } from '../app.component';
import { ListingService } from '../services/listing.service';
import { Location } from '@angular/common';
import { environment } from 'src/environments/environment';
import { FETCH_USER, UPDATE_USER } from '../constants/url-strings';
import { User } from '../interfaces/user';
import {
  DURATION,
  UNABLE_TO_UPDATE_USER,
  USER_UPDATED,
} from '../constants/strings';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.page.html',
  styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage implements OnInit {
  uuid = '';
  user: User;

  form = new FormGroup({
    name: new FormControl(),
    email: new FormControl(),
    password: new FormControl(),
    phone: new FormControl(),
    role: new FormControl(),
    dateOfJoining: new FormControl(),
    drivesAttended: new FormControl(),
  });
  constructor(
    private readonly route: ActivatedRoute,
    private readonly appComponent: AppComponent,
    private readonly listingService: ListingService,
    private readonly router: Router,
    private readonly toastController: ToastController,
    private location: Location
  ) {}

  ngOnInit() {
    this.uuid = this.route.snapshot.params.uuid;
    if (this.uuid) {
      this.getUserData();
    } else {
      this.router.navigate['/list-users'];
    }
  }

  navigateBack() {
    this.location.back();
  }

  getUserData() {
    const url = environment.serverUrl + FETCH_USER + this.uuid;
    this.appComponent.getAccessToken().then((token) => {
      this.listingService.getRequest(url, undefined, token).subscribe({
        next: (success) => {
          this.user = success;
          console.log(success);
          this.setFormValues(success);
        },
        error: (error) => {
          console.log(error);
        },
      });
    });
  }

  setFormValues(payload: User) {
    this.form.get('name').setValue(payload.name);
    this.form.get('email').setValue(payload.email);
    this.form.get('password').setValue(payload.password);
    this.form.get('phone').setValue(payload.phone);
    this.form.get('role').setValue(payload.role);
    this.form
      .get('dateOfJoining')
      .setValue(new Date(payload.dateOfJoining).toISOString().substring(0, 10));
    this.form.get('drivesAttended').setValue(payload.drivesAttended);
  }

  update() {
    const user = {} as User;
    user.uuid = this.uuid;
    user.name = this.form.get('name').value;
    user.email = this.form.get('email').value;
    user.phone = this.form.get('phone').value;
    // user.password = this.form.get('password').value;
    console.log(user);
    const url = environment.serverUrl + UPDATE_USER;

    this.listingService.postRequest(url, user).subscribe({
      next: (success) => {
        console.log(success);
        this.presentToast(USER_UPDATED);
      },
      error: (error) => {
        console.log(error);
        this.presentToast(UNABLE_TO_UPDATE_USER);
      },
    });
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: DURATION,
    });
    toast.present();
  }

  selectImage() {}
}
