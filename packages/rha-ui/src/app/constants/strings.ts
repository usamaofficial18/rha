export const DARK_MODE = 'dark_mode';
export const DARK_MODE_CLASS_NAME = 'darkMode';
export const ON = 'on';
export const OFF = 'off';
export const DURATION = 2000;

export const EVENT_CREATED = 'Event Created';
export const UNABLE_TO_CREATE_EVENT = 'Unable to create event';
export const UNABLE_TO_FETCH_EVENTS = 'Unable to fetch events';
export const UNABLE_TO_UPDATE_EVENT = 'Unable to update event';
export const EVENT_UPDATED = 'Event Updated';

export const USER_ADDED = 'User added';
export const USER_UPDATED = 'User updated';
export const UNABLE_TO_ADD_USER = 'Unable to add user';
export const UNABLE_TO_UPDATE_USER = 'Unable to update user';
export const UNABLE_TO_FETCH_USERS = 'Unable to fetch users';

export const ACCESS_TOKEN = 'access_token';

export const AUTHORIZATION = 'Authorization';
export const BEARER_HEADER_PREFIX = 'Bearer ';
