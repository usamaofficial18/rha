export class Occasion {
  uuid?: string;
  title?: string;
  description?: string;
  status?: boolean;
  date?: Date;
  startTime?: string;
  endTime?: string;
}
