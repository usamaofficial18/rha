import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'edit-event/:uuid',
    loadChildren: () =>
      import('./pages/admin/create-occasion/create-occasion.module').then(
        (m) => m.CreateOccasionPageModule
      ),
  },
  {
    path: 'create-event',
    loadChildren: () =>
      import('./pages/admin/create-occasion/create-occasion.module').then(
        (m) => m.CreateOccasionPageModule
      ),
  },
  {
    path: 'view-events',
    loadChildren: () =>
      import('./pages/view-occasions/view-occasions.module').then(
        (m) => m.ViewOccasionsPageModule
      ),
  },

  {
    path: 'view-occasions',
    loadChildren: () =>
      import('./pages/view-occasions/view-occasions.module').then(
        (m) => m.ViewOccasionsPageModule
      ),
  },
  {
    path: 'add-user',
    loadChildren: () =>
      import('./pages/admin/add-user/add-user.module').then(
        (m) => m.AddUserPageModule
      ),
  },
  {
    path: 'page-one',
    loadChildren: () =>
      import('./page-one/page-one.module').then((m) => m.PageOnePageModule),
  },
  {
    path: 'settings',
    loadChildren: () =>
      import('./page-two/page-two.module').then((m) => m.PageTwoPageModule),
  },
  {
    path: 'page-three',
    loadChildren: () =>
      import('./page-three/page-three.module').then(
        (m) => m.PageThreePageModule
      ),
  },
  {
    path: 'list-users',
    loadChildren: () =>
      import('./pages/list-users/list-users.module').then(
        (m) => m.ListUsersPageModule
      ),
  },
  {
    path: 'login',
    loadChildren: () =>
      import('./pages/login/login.module').then((m) => m.LoginPageModule),
  },
  {
    path: 'register',
    loadChildren: () =>
      import('./pages/register/register.module').then(
        (m) => m.RegisterPageModule
      ),
  },
  {
    path: 'profile/:uuid',
    loadChildren: () =>
      import('./pages/profile/profile.module').then((m) => m.ProfilePageModule),
  },
  {
    path: 'leaderboard',
    loadChildren: () =>
      import('./pages/leaderboard/leaderboard.module').then(
        (m) => m.LeaderboardPageModule
      ),
  },
  {
    path: 'splash-screen',
    loadChildren: () =>
      import('./pages/splash-screen/splash-screen.module').then(
        (m) => m.SplashScreenPageModule
      ),
  },
  {
    path: 'dashboard',
    loadChildren: () =>
      import('./pages/dashboard/dashboard.module').then(
        (m) => m.DashboardPageModule
      ),
  },
  {
    path: 'edit-profile/:uuid',
    loadChildren: () =>
      import('./edit-profile/edit-profile.module').then(
        (m) => m.EditProfilePageModule
      ),
  },
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: '**', redirectTo: 'dashboard' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
