import { OverlayContainer } from '@angular/cdk/overlay';
import { ApplicationRef, Component, HostBinding, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { DARK_MODE, DARK_MODE_CLASS_NAME, OFF, ON } from '../constants/strings';
import { StorageService } from '../services/storage.service';

@Component({
  selector: 'app-page-two',
  templateUrl: './page-two.page.html',
  styleUrls: ['./page-two.page.scss'],
})
export class PageTwoPage implements OnInit {
  @HostBinding() class = '';
  toggleControl = new FormControl(false);
  modeSwitch = 0;

  constructor(
    private readonly storage: StorageService,
    private readonly overlay: OverlayContainer,
    private readonly ref: ApplicationRef
  ) {}

  slideOpts = {
    initialSlide: 1,
    speed: 400,
  };

  ngOnInit() {
    //toggle switch
    this.storage.getItem(DARK_MODE).then((darkMode) => {
      if (darkMode === ON) {
        this.modeSwitch = 1;
      } else {
        this.modeSwitch = 0;
      }
    });

    this.storage.getItem(DARK_MODE).then((darkMode) => {
      if (darkMode === ON) {
        this.class = DARK_MODE_CLASS_NAME;
        this.overlay.getContainerElement().classList.add(this.class);
        document.body.setAttribute('color-theme', 'dark');
        this.ref.tick();
      } else {
        this.class = '';
        this.overlay
          .getContainerElement()
          .classList.remove(DARK_MODE_CLASS_NAME);
        document.body.setAttribute('color-theme', 'light');
        this.ref.tick();
      }
    });
  }

  onToggleColorTheme(event: any) {
    if (event.detail.checked) {
      this.storage.setItem(DARK_MODE, ON);
      document.body.setAttribute('color-theme', 'dark');
      this.ref.tick();
    } else {
      this.storage.setItem(DARK_MODE, OFF);
      document.body.setAttribute('color-theme', 'light');
      this.ref.tick();
    }
  }
}
