import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PageTwoPageRoutingModule } from './page-two-routing.module';

import { PageTwoPage } from './page-two.page';
import { MaterialModule } from '../material.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MaterialModule,
    PageTwoPageRoutingModule
  ],
  declarations: [PageTwoPage]
})
export class PageTwoPageModule {}
