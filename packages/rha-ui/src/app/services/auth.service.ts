import { Injectable } from '@angular/core';
import { StorageService } from './storage.service';
import jwt_decode from 'jwt-decode';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private readonly storage: StorageService) {}

  async getUserFromToken(token: string) {
    const user: any = jwt_decode(token);
    return user.user.uuid;
  }
}
