import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router, RouterEvent } from '@angular/router';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-page-one',
  templateUrl: './page-one.page.html',
  styleUrls: ['./page-one.page.scss'],
})
export class PageOnePage implements OnInit {
  id = '';
  constructor(private appComponent: AppComponent, private router: Router) {}

  async ngOnInit() {
    this.id = await this.appComponent.getIdFromToken();
    this.router.events.subscribe({
      next: async (event: RouterEvent) => {
        if (event instanceof NavigationEnd) {
          this.id = await this.appComponent.getIdFromToken();
        }
      },
    });
  }

  logout() {
    this.appComponent
      .removeAccessToken()
      .then(() => this.router.navigate(['/']));
  }
}
