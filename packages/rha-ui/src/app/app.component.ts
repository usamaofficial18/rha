import { ApplicationRef, Component, HostBinding, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { StorageService } from './services/storage.service';
import {
  ACCESS_TOKEN,
  DARK_MODE,
  DARK_MODE_CLASS_NAME,
  OFF,
  ON,
} from './constants/strings';
import { OverlayContainer } from '@angular/cdk/overlay';
import { CreateOccasionPage } from './pages/admin/create-occasion/create-occasion.page';
import { ViewOccasionsPage } from './pages/view-occasions/view-occasions.page';
import { AddUserPage } from './pages/admin/add-user/add-user.page';
import { PageOnePage } from './page-one/page-one.page';
import { PageTwoPage } from './page-two/page-two.page';
import { PageThreePage } from './page-three/page-three.page';
import jwt_decode from 'jwt-decode';
import { User } from './interfaces/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'rha-ui';
  @HostBinding() class = '';

  constructor(
    private readonly storage: StorageService,
    private readonly overlay: OverlayContainer,
    private readonly ref: ApplicationRef
  ) {}

  slideOpts = {
    initialSlide: 1,
    speed: 400,
  };

  ngOnInit() {
    this.storage.getItem(DARK_MODE).then((darkMode) => {
      if (darkMode === ON) {
        this.class = DARK_MODE_CLASS_NAME;

        this.overlay.getContainerElement().classList.add(this.class);
        document.body.setAttribute('color-theme', 'dark');
        this.ref.tick();
      } else {
        this.class = '';
        this.overlay
          .getContainerElement()
          .classList.remove(DARK_MODE_CLASS_NAME);
        document.body.setAttribute('color-theme', 'light');
        this.ref.tick();
      }
    });
  }

  storeAccessToken(token: string) {
    this.storage.setItem(ACCESS_TOKEN, token);
  }

  getAccessToken() {
    return this.storage.getItem(ACCESS_TOKEN);
  }

  removeAccessToken() {
    return this.storage.removeItem(ACCESS_TOKEN);
  }

  async getIdFromToken() {
    const accessToken = await this.getAccessToken();
    if (accessToken) {
      const user: any = jwt_decode(await this.getAccessToken());
      return user.user.uuid;
    }
  }
}
